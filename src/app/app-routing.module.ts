import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './components/menu/menu.component';
import { ProductoComponent } from './components/producto/producto.component';
import { CanalComponent } from './components/canal/canal.component';
import { DistribuidorComponent } from './components/distribuidor/distribuidor.component';
import { AddDistribuidorComponent } from './components/add-distribuidor/add-distribuidor.component';
import { EditDistribuidorComponent } from './components/edit-distribuidor/edit-distribuidor.component';
import { AddProductoComponent } from './components/add-producto/add-producto.component';
import { EditProductoComponent } from './components/edit-producto/edit-producto.component';

const routes: Routes = [

{
  path:'menu',
  component:MenuComponent
},
{
  path:'canales',
  component:CanalComponent
},
{
  path:'distribuidores',
  component:DistribuidorComponent
},
{
  path:'add-distribuidores',
  component:AddDistribuidorComponent
},
{
  path:'edit-distribuidores/:id',
  component:EditDistribuidorComponent
},
{
  path:'productos',
  component:ProductoComponent
},
{
  path:'add-productos',
  component:AddProductoComponent
},
{
  path:'edit-productos/:id',
  component:EditProductoComponent
},


{
  path : '**', redirectTo: 'menu'
}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
