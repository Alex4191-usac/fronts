import { Component, OnInit } from '@angular/core';
import { producto } from 'src/app/interfaces/producto';
import { CdpServiceService } from 'src/app/services/cdp-service.service';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { faPen } from '@fortawesome/free-solid-svg-icons';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  productos: producto[]=[];
  faPlus = faPlus;
  faTrash =faTrash;
  faPen=faPen;
  constructor(private _cpd: CdpServiceService) { 

    this.getProductos();

  }

  ngOnInit(): void {
  }

  getProductos():void{
    this._cpd.getProductos().subscribe((resp: any) => {
      if(resp!=null){
        this.productos = resp;
        
      }
      
    });
  }

  public onDeleteCanal(codigoDistribuidor: number): void{
    this._cpd.deleteProducto(codigoDistribuidor).subscribe(
      (response: any) => {
        this.getProductos()
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
       
      }
    );

  }


}
