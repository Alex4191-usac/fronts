import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { distribuidor } from 'src/app/interfaces/distribuidor';
import { CdpServiceService } from 'src/app/services/cdp-service.service';

@Component({
  selector: 'app-add-producto',
  templateUrl: './add-producto.component.html',
  styleUrls: ['./add-producto.component.css']
})
export class AddProductoComponent implements OnInit {
  formularioProducto: FormGroup;
  distribuidores: distribuidor[]=[];
  constructor(private _cpd: CdpServiceService,
    public formulario:FormBuilder, private router: Router) { 

      this.getDistribuidores();
      this.formularioProducto=this.formulario.group({
        descripcion:"",
        monto:0,
        distribuidor:[]
    });




    }

  ngOnInit(): void {
  }

  getDistribuidores():void{
  
    this._cpd.GetDistribuidores().subscribe((resp: any) => {
      if(resp!=null){
        this.distribuidores = resp;
        console.log(this.distribuidores)
      }
      
    });

  }

  sendDatos():any{
    
    this._cpd.addProducto(this.formularioProducto.value).subscribe(
      (response: any) => {
        
        this.router.navigate(['productos'])
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        
      }
    );

  }


}
