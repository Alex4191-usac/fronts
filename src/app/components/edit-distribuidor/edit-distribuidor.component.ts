import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup,FormBuilder } from '@angular/forms';
import { canal } from 'src/app/interfaces/canal';
import { CdpServiceService } from 'src/app/services/cdp-service.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-edit-distribuidor',
  templateUrl: './edit-distribuidor.component.html',
  styleUrls: ['./edit-distribuidor.component.css']
})
export class EditDistribuidorComponent implements OnInit {
  formularioDistribuidor: FormGroup;
  elID:any;
  canales: canal[]=[];
 
  constructor(private activeRoute:ActivatedRoute,
    private _cpd: CdpServiceService,  public formulario:FormBuilder,
    private router: Router) {
    this.elID=this.activeRoute.snapshot.paramMap.get('id');
    console.log(this.elID);
    
    this.getCanales();

    this._cpd.getDistribuidor(this.elID).subscribe((resp: any) => {
      if(resp!=null){
        this.formularioDistribuidor.setValue({
          nombre:resp.nombre,
          calerta:resp.calerta,
          cnotificacion:resp.cnotificacion,
          cActual:resp.canalAutorizado.nombreCanal,
          canalAutorizado:[]
          
        })
      }
      
    });

    this.formularioDistribuidor=this.formulario.group({
      nombre:"",
      calerta:"",
      cnotificacion:"",
      cActual:"",
      canalAutorizado:[]
  });

   }

  ngOnInit(): void {
  }

  getCanales():void{
  
    this._cpd.GetCanales().subscribe((resp: any) => {
      if(resp!=null){
        this.canales = resp;
        console.log(this.canales)
      }
      
    });

  }

  enviarDatos():any{
    this._cpd.modificarDistribuidor(this.formularioDistribuidor.value,this.elID).subscribe(
      (response: any) => {
        console.log(response);
        this.router.navigate(['distribuidores'])
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        
      }
    );
  }

}
