import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';
import { distribuidor } from 'src/app/interfaces/distribuidor';
import { CdpServiceService } from 'src/app/services/cdp-service.service';

@Component({
  selector: 'app-edit-producto',
  templateUrl: './edit-producto.component.html',
  styleUrls: ['./edit-producto.component.css']
})
export class EditProductoComponent implements OnInit {
  formularioProducto: FormGroup;
  distribuidores: distribuidor[]=[];
  elID:any;
  constructor(private _cpd: CdpServiceService,
    public formulario:FormBuilder, private router: Router,
    private activeRoute:ActivatedRoute) { 
      
      this.elID=this.activeRoute.snapshot.paramMap.get('id');
      console.log(this.elID);
      this.getDistribuidores();

      this._cpd.getProducto(this.elID).subscribe((resp: any) => {
        if(resp!=null){
          this.formularioProducto.setValue({
            descripcion:resp.descripcion,
            monto:resp.monto,
            dActual:resp.distribuidor.nombre,
            distribuidor:[]
            
          })
        }
      });

      this.formularioProducto=this.formulario.group({
        descripcion:"",
        monto:0,
        dActual:"",
        distribuidor:[]
    });




    }

  ngOnInit(): void {
  }


  getDistribuidores():void{
  
    this._cpd.GetDistribuidores().subscribe((resp: any) => {
      if(resp!=null){
        this.distribuidores = resp;
        console.log(this.distribuidores)
      }
      
    });

  }

  enviarDatos():any{
    this._cpd.modificarProducto(this.formularioProducto.value,this.elID).subscribe(
      (response: any) => {
        console.log(response);
        this.router.navigate(['productos'])
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        
      }
    )
  }


}
