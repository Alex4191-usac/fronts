import { Component, OnInit } from '@angular/core';
import { distribuidor } from 'src/app/interfaces/distribuidor';
import { CdpServiceService } from 'src/app/services/cdp-service.service';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { faPen } from '@fortawesome/free-solid-svg-icons';
import { HttpErrorResponse } from '@angular/common/http';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-distribuidor',
  templateUrl: './distribuidor.component.html',
  styleUrls: ['./distribuidor.component.css']
})
export class DistribuidorComponent implements OnInit {
  faPlus = faPlus;
  faTrash =faTrash;
  faPen=faPen;
  distribuidores: distribuidor[]=[];
  constructor(private _cpd: CdpServiceService) {
    this.getDistribuidores();
   }

  ngOnInit(): void {
  }



  getDistribuidores():void{
    this._cpd.GetDistribuidores().subscribe((resp: any) => {
      if(resp!=null){
        this.distribuidores = resp;
        console.log(this.distribuidores)
      }
      
    });
  }


  public onDeleteCanal(codigoDistribuidor: number): void{
    this._cpd.deleteDistribuidor(codigoDistribuidor).subscribe(
      (response: any) => {
        this.getDistribuidores();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
       
      }
    );

  }
}
