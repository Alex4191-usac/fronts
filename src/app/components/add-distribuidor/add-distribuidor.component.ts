import { Component, OnInit } from '@angular/core';
import { canal } from 'src/app/interfaces/canal';
import { CdpServiceService } from 'src/app/services/cdp-service.service';
import { FormGroup,FormBuilder } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-distribuidor',
  templateUrl: './add-distribuidor.component.html',
  styleUrls: ['./add-distribuidor.component.css']
})
export class AddDistribuidorComponent implements OnInit {
  formularioDistribuidor: FormGroup;
  canales: canal[]=[];
  constructor(private _cpd: CdpServiceService,
    public formulario:FormBuilder, private router: Router) {

    this.getCanales(); 
    this.formularioDistribuidor=this.formulario.group({
        nombre:"",
        calerta:"",
        cnotificacion:"",
        canalAutorizado:[]
    });
   }

  ngOnInit(): void {
  }

  sendDatos():any{
    console.log(this.formularioDistribuidor.value);
    this._cpd.addDistribuidor(this.formularioDistribuidor.value).subscribe(
      (response: any) => {
        console.log(response);
        this.router.navigate(['distribuidores'])
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        
      }
    );

  }


  


 

  getCanales():void{
  
    this._cpd.GetCanales().subscribe((resp: any) => {
      if(resp!=null){
        this.canales = resp;
        console.log(this.canales)
      }
      
    });

  }

}
