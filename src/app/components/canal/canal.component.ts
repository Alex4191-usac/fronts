import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { CdpServiceService } from 'src/app/services/cdp-service.service';
import { canal } from 'src/app/interfaces/canal';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-canal',
  templateUrl: './canal.component.html',
  styleUrls: ['./canal.component.css']
})
export class CanalComponent implements OnInit {

  nombreCanal: String ="NombreCanal";
  canalObjeto ={
    codigoCanal:0,
    nombreCanal:""
  };
  canales: canal[]=[];
  isEdit: boolean = false;
  title = 'ng-bootstrap-modal-demo';
  closeResult: string="";
 
   constructor(private _cpd: CdpServiceService) {

    this.getCanales(); 
   
   }

  ngOnInit(): void {
  }

  getCanales():void{
  
    this._cpd.GetCanales().subscribe((resp: any) => {
      if(resp!=null){
        this.canales = resp;
        console.log(this.canales)
      }
      
    });

  }


  public onAddCanal(addForm: NgForm): void {
    this._cpd.addPersona(addForm.value).subscribe(
      (response: any) => {
        this.getCanales();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );

  }

  public onDeleteCanal(codigoCanal: number): void{
    this._cpd.deleteCanal(codigoCanal).subscribe(
      (response: any) => {
        this.getCanales();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
       
      }
    );

  }

  
  public onEdit(canal:any) {
    this.isEdit=true;
    this.canalObjeto.codigoCanal  = canal.codigoCanal;
    this.canalObjeto.nombreCanal = canal.nombreCanal;
   
    this.nombreCanal = canal.nombreCanal;
  }

  public cancel(){
    this.isEdit=false;
    this.nombreCanal = "";
  }

  public onUpdateCanal(nombreCanal: string){
   
    this.canalObjeto.nombreCanal = nombreCanal;
    console.log( 'canal editado:',  this.canalObjeto);
    const canalN = {
      codigoCanal: this.canalObjeto.codigoCanal,
      nombreCanal: this.canalObjeto.nombreCanal
    }
    
    this._cpd.ModificarCanal(canalN, this.canalObjeto.codigoCanal).subscribe(
      (response: string) => {
        console.log(response);
        this.getCanales();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );

  }

/*this._cpd.ModificarCanal(canal,codigoCanal).subscribe(
      (response: string) => {
        console.log(response);
        this.getCanales();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    ); */



}


