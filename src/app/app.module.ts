import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { ProductoComponent } from './components/producto/producto.component';
import { DistribuidorComponent } from './components/distribuidor/distribuidor.component';
import { CanalComponent } from './components/canal/canal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { BackButtonComponent } from './components/back-button/back-button.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AddDistribuidorComponent } from './components/add-distribuidor/add-distribuidor.component';
import { EditDistribuidorComponent } from './components/edit-distribuidor/edit-distribuidor.component';
import { EditProductoComponent } from './components/edit-producto/edit-producto.component';
import { AddProductoComponent } from './components/add-producto/add-producto.component';
@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ProductoComponent,
    DistribuidorComponent,
    CanalComponent,
    BackButtonComponent,
    AddDistribuidorComponent,
    EditDistribuidorComponent,
    EditProductoComponent,
    AddProductoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SweetAlert2Module,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
