import { TestBed } from '@angular/core/testing';

import { CdpServiceService } from './cdp-service.service';

describe('CdpServiceService', () => {
  let service: CdpServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CdpServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
