import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { canal } from '../interfaces/canal';
import { distribuidor } from '../interfaces/distribuidor';
import { producto } from '../interfaces/producto';
@Injectable({
  providedIn: 'root'
})
export class CdpServiceService {

  
  constructor(private http:HttpClient) { }

  apiURL = 'http://localhost:8080/api'


  //canales
  GetCanales(){  
    return this.http.get(this.apiURL+'/canales');
  }

  public addPersona(canal: canal){
    return this.http.post(`${this.apiURL}/canales`,canal, {responseType: 'text'});
  }

  public deleteCanal(codigoCanal: number) {
    return this.http.delete(`${this.apiURL}/canales/${codigoCanal}`);
  }

  public ModificarCanal(canal:canal, codigoCanal:number){
    return this.http.put(`${this.apiURL}/canales/actualizar/${codigoCanal}`,canal, {responseType: 'text'})

  }

  GetDistribuidores(){
    return this.http.get(this.apiURL+'/distribuidores');
  }


  public deleteDistribuidor(codigoDistribuidor: number){
    return this.http.delete(`${this.apiURL}/distribuidores/${codigoDistribuidor}`)
  }

  public addDistribuidor(distribuidor: distribuidor){
    return this.http.post(`${this.apiURL}/distribuidores`,distribuidor, {responseType: 'text'});
  }

  public getDistribuidor(codigoDistribuidor:number){
    return this.http.get(`${this.apiURL}/distribuidores/${codigoDistribuidor}`)
  }

  public modificarDistribuidor(distribuidor:distribuidor,codigoDistribuidor:number){
    return this.http.put(`${this.apiURL}/distribuidores/update/${codigoDistribuidor}`, distribuidor,{responseType: 'text'});
  }


  getProductos(){
    return this.http.get(this.apiURL+'/productos');
  }

  public deleteProducto(codigoProducto: number){
    return this.http.delete(`${this.apiURL}/productos/${codigoProducto}`)
  }


  public addProducto(producto: producto){
    return this.http.post(`${this.apiURL}/productos`,producto, {responseType: 'text'});
  }

  public getProducto(codigoProducto:number){
    return this.http.get(`${this.apiURL}/productos/${codigoProducto}`);
  }

  public modificarProducto(producto:producto,codigoProducto:number){
    return this.http.put(`${this.apiURL}/productos/update/${codigoProducto}`, producto,{responseType: 'text'});

  }



  

}
